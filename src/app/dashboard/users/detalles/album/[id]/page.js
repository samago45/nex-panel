'use client'
import {UsuarioServices} from "@/app/services/usuarios/UsuarioServices";
import {useEffect, useState} from "react";
import Album from "@/components/Album";
import {Spinner} from "flowbite-react";

export default function Page({params}) {
    const [photos, setPhotos] = useState([]);
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        setLoading(true);
        UsuarioServices.obtenerAlbum(params.id)
            .then(response => {
                setPhotos(response);
            })
            .catch(error => {
                console.error('Error al obtener el usuario:', error);
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);


    return (<>

        {loading ? (<div
            className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-900 bg-opacity-50">
            <Spinner size="xl"/>
        </div>) : (<div
                className=" text-center mx-auto max-w-screen-xl px-2 lg:px-12  bg-white border border-gray-200 rounded-lg dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">

                <h2 className=" mt-6 mb-4 text-xl mx:text-center font-bold text-blue-600 dark:text-white"> Album de
                    Imágenes
                </h2>
                <Album data={photos}/>
            </div>


        )}
    </>)
}
