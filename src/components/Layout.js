'use client'
import {usePathname} from 'next/navigation';
import React, {useEffect, useState} from 'react'
import {initFlowbite} from 'flowbite';
import Aside from "@/components/Aside";
import Navbar from "@/components/Navbar";
import {ignorePaths} from "@/utils/utils";

export default function Layout({children}) {
    const [activeLink, setActiveLink] = useState('');
    const pathname = usePathname();
    useEffect(() => {

        setActiveLink(pathname);

        initFlowbite();
    }, [pathname]);


    const shouldRenderNavBar = !ignorePaths.some((path) => pathname.startsWith(path));

    return (<div>

        {shouldRenderNavBar && (<div>
            <Navbar />
            <Aside activeLink={activeLink}/>
        </div>)}


        <div className={`${!shouldRenderNavBar ? "sm:ml-0" : " sm:ml-64"} p-2 mt-14 mx-auto lg:px-12 px-2 `}>
            {children}
        </div>
    </div>);
}
