import axios from "axios";
import apiConfig from "@/app/services/apiConfig";


export const UsuarioServices = {


    async obtenerUsuarios() {
        try {
            const response = await axios.get(`${apiConfig.usuarios}`);
            return response.data;
        } catch (error) {
            throw new Error(`Error al obtener usuarios: ${error.message}`);
        }
    }, async obtenerUsuariosId(id) {
        try {
            const response = await axios.get(`${apiConfig.usuarios}/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(`Error al obtener usuario: ${error.message}`);
        }
    },


    async obtenerDetalles(id) {
        try {
            const response = await axios.get(`${apiConfig.usuarios}/${id}/albums`);
            return response.data;
        } catch (error) {
            throw new Error(`Error al obtener usuario: ${error.message}`);
        }
    },

    async obtenerAlbum(id) {
        try {
            const response = await axios.get(`${apiConfig.albums}/${id}/photos`);
            return response.data;
        } catch (error) {
            throw new Error(`Error al obtener usuario: ${error.message}`);
        }
    },

    async obtenerPost() {
        try {
            const response = await axios.get(`${apiConfig.posts}`);
            return response.data;
        } catch (error) {
            throw new Error(`Error al obtener usuarios: ${error.message}`);
        }
    }


}

