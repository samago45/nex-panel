"use client";


export default function Album({data}) {
    return (


        <div className="grid grid-cols-2 p-8 md:grid-cols-3 gap-4">
            {data.map((item, index) => (<div key={index}>
                <img
                    className="h-auto max-w-full rounded-lg"
                    src={item.url}
                    alt={`Image ${index + 1}`}
                />
                <span className="text-center mt-2 text-neutral-950 text-sm">{item.title}</span>
            </div>))}
        </div>);
};



