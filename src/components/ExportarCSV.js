'use client'
import React, {useState} from 'react';
import {parse} from 'json2csv';

export default function ExportarCSV({data}) {
    const exportToCsv = () => {
        if (data.length === 0) {
            console.warn('No hay datos para exportar');
            return;
        }

        try {
            const primerElemento = data[0];
            const columnas = Object.keys(primerElemento);
            const csv = parse(data, {fields: columnas});
            const blob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});

            const url = window.URL.createObjectURL(blob);

            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'datos.csv');
            document.body.appendChild(link);
            link.click();

            // Liberar el objeto URL creado
            window.URL.revokeObjectURL(url);
            document.body.removeChild(link);
        } catch (error) {
            console.error('Error al exportar a CSV:', error);
        }
    };

    return (

        <button type="button" onClick={exportToCsv}
                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            Generar Excel

        </button>


    );
};


