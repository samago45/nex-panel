'use client';
import {useRouter} from 'next/navigation';
import {Label} from "flowbite-react";

export default function Login() {
    const {push} = useRouter();

    const handleClick = () => {
        push('/dashboard/users')

    }

    return (<div className="container mx-auto py-8">
            <form className="w-full max-w-sm mx-auto bg-white p-8 rounded-md shadow-md">
                <h2 className="text-md mb-6 text-center text-gray-400">Iniciar sesión</h2>
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
                        Correo Electronico
                    </label>
                    <input
                        className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                        type="email"
                        id="email"
                        name="email"
                        placeholder="Correo Electronico"
                    />
                </div>
                <div className="mb-4">
                    <Label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="password"
                    >
                        Contraseña
                    </Label>
                    <input
                        className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                        type="password"
                        id="password"
                        name="password"
                        placeholder="********"
                    />
                </div>
                <div className="flex items-center justify-between my-5">
                    <div className="flex items-start">
                        <div className="flex items-center h-5">
                            <input
                                id="remember"
                                aria-describedby="remember"
                                type="checkbox"
                                className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
                                required=""
                            />
                        </div>
                        <div className="ml-3 text-sm">
                            <label htmlFor="remember" className="text-gray-500 dark:text-gray-300">
                                Recordar credenciales
                            </label>
                        </div>
                    </div>
                </div>
                <button
                    className="w-full bg-blue-700 text-white text-sm font-bold py-3 px-4 rounded-md hover:bg-blue-700 transition duration-300"
                    type="button"
                    onClick={handleClick}
                >
                    Inicar sesión
                </button>
            </form>
        </div>);
}
