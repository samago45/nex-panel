"use client"

import {UsuarioServices} from "@/app/services/usuarios/UsuarioServices";
import {useEffect, useState} from "react";
import GraficoBarras from "@/components/GraficoBarras";
import {generateRandomColors} from "@/utils/utils";
import {Spinner} from "flowbite-react";

export default function Page() {
    const [publicaciones, setPublicaciones] = useState([]);
    const [usuarios, setUsuarios] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        Promise.all([UsuarioServices.obtenerUsuarios(), UsuarioServices.obtenerPost()])
            .then(([usuarios, publicaciones]) => {
                setUsuarios(usuarios);
                setPublicaciones(publicaciones);
            })
            .catch(error => {
                console.error('Error al obtener usuarios o publicaciones:', error);
            }).finally(() => {
            setLoading(false); // Establecer carga como falso después de la petición
        });
    }, []);


    // Combinar los datos de usuarios y publicaciones
    const publicacionesPorUsuario = {};
    publicaciones.forEach((publicacion) => {
        const {userId} = publicacion;
        if (userId in publicacionesPorUsuario) {
            publicacionesPorUsuario[userId]++;
        } else {
            publicacionesPorUsuario[userId] = 1;
        }
    });

    // Preparar los datos para el gráfico de barras
    const labels = usuarios.map(usuario => usuario.name);
    const dataGrafico = usuarios.map(usuario => publicacionesPorUsuario[usuario.id] || 0);
    const colores = generateRandomColors(usuarios.length);
    const datosGrafico = {
        labels: labels, datasets: [{
            label: 'Cantidad de Publicaciones por Usuario',
            data: dataGrafico,
            backgroundColor: colores.map(color => `rgba(${color.r}, ${color.g}, ${color.b}, 0.2)`),
            borderColor: colores.map(color => `rgba(${color.r}, ${color.g}, ${color.b}, 1)`),
            borderWidth: 1
        }]
    };

    return (<>
        {loading ? (<div
            className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-900 bg-opacity-50">
            <Spinner size="xl"/>
        </div>) : (<div
            className=" text-center mx-auto max-w-screen-xl px-2 lg:px-12  bg-white border border-gray-200 rounded-lg dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <h2 className=" mt-6 mb-4 text-xl mx:text-center font-bold text-blue-600 dark:text-white"> Cantidad
                de
                Publicaciones por Usuario
            </h2>

            <GraficoBarras datos={datosGrafico}/>

        </div>)}
    </>)
}


