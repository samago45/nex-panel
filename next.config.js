/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  async redirects() {
    return [
      {
        source: '/dashboard',
        destination: '/dashboard/users',
        permanent: true,
      },
      {
        source: '/dashboard',
        destination: '/dashboard/reporte',
        permanent: true,
      },
      {
        source: '/dashboard',
        destination: '/dashboard/album/[id]',
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig
