'use client'
import React from "react";

const TablaIetms = ({items = [], columnas = []}) => {
    return (<table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
        <tr>
            {columnas.map((columna, index) => (<th key={index} scope="col" className="p-4">
                {columna.name}
            </th>))}
        </tr>
        </thead>
        <tbody>
        {items.map((item, itemIndex) => (<tr
            key={itemIndex}
            className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
        >
            {columnas.map((columna, columnIndex) => (<td key={columnIndex} className="px-6 py-4">
                {columna.selector ? columna.selector(item) : ''}
            </td>))}
        </tr>))}
        </tbody>
    </table>);
}

export default TablaIetms;
