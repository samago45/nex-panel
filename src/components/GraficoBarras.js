import React, {useEffect, useRef} from 'react';
import Chart from 'chart.js/auto';

export default function GraficoBarras({datos}) {
    const canvasRef = useRef(null);
    let grafico = null;

    useEffect(() => {
        if (canvasRef.current && datos && datos.labels && datos.datasets) {
            grafico = new Chart(canvasRef.current, {
                type: 'bar', data: datos, options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        }

        return () => {
            if (grafico !== null) {
                grafico.destroy();
            }
        };
    }, [datos]);

    return <canvas ref={canvasRef}/>;
};


