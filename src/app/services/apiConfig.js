const baseUrl = 'https://jsonplaceholder.typicode.com'


const apiConfig = {
    baseUrl,

    usuarios: `${baseUrl}/users`,
    albums: `${baseUrl}/albums`,
    posts: `${baseUrl}/posts`,

}

export default apiConfig
