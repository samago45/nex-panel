"use client"
import {UsuarioServices} from "@/app/services/usuarios/UsuarioServices";
import {useEffect, useState} from "react";
import {useForm} from "react-hook-form";
import {Spinner} from "flowbite-react";
import TablaIetms from "@/components/TablaIetms";
import {useRouter} from "next/navigation";

const datosUsuario = (data) => {
    return {
        name: data.name,
        phone: data.phone,
        username: data.username,
        email: data.email,
        city: data.address.city,
        companyName: data.company.name,
        bs: data.company.bs,
        catchPhrase: data.company.catchPhrase,


    };
};
export default function Page({params}) {
    const router = useRouter();
    const [usuarios, setUsuarios] = useState([]);
    const [album, setAlbum] = useState([])
    const [loading, setLoading] = useState(false)
    const {reset, register} = useForm()

    useEffect(() => {
        setLoading(true); // Establecer carga como verdadero antes de la petición

        UsuarioServices.obtenerUsuariosId(params.id)
            .then(response => {
                setUsuarios(response);
                reset(datosUsuario(response));

            })
            .catch(error => {
                console.error('Error al obtener el usuario:', error);
            })
            .finally(() => {
                setLoading(false); // Establecer carga como falso después de la petición
            });
    }, []);


    useEffect(() => {
        setLoading(true); // Establecer carga como verdadero antes de la petición

        UsuarioServices.obtenerDetalles(params.id)
            .then(response => {
                setAlbum(response);
            })
            .catch(error => {
                console.error('Error al obtener el usuario:', error);
            })
            .finally(() => {
                setLoading(false); // Establecer carga como falso después de la petición
            });
    }, []);


    const handleSelect = (id) => {
        router.push(`/dashboard/users/detalles/album/${id}`);
    }


    const columnas = [{name: "Album", selector: (row) => row.title}, {
        name: 'Acciones', selector: (row) => (<>

            <div>
                <button type="button" onClick={() => handleSelect(row.id)}
                        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    Ver album
                    <svg className="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true"
                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                              d="M1 5h12m0 0L9 1m4 4L9 9"/>
                    </svg>
                </button>
            </div>
        </>),
    },];

    return (<div>

        {loading ? (<div
            className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-900 bg-opacity-50">
            <Spinner size="xl"/>
        </div>) : (<div
            className="  mx-auto max-w-screen-xl px-2 lg:px-12  bg-white border border-gray-200 rounded-lg dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">

            <div className=" py-8 mx-4">
                <h2 className="mb-4 text-xl font-bold text-gray-900 dark:text-white">Usuario <span
                    className="text-blue-600 mb-8">
                    {usuarios.username}
                         </span></h2>
                <div className="flex flex-col gap-x-2 mb-4 sm:flex-row sm:justify-start gap-y-2   justify-center ">
                    <div
                        className="sm:mx-auto max-w-screen-xl sm:px-2 lg:px-12 p-12 sm:p-8  bg-white border border-gray-200 rounded-lg  dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                        <h2 className="mb-4 text-xl font-normal text-gray-900 dark:text-white">Información
                            Personales </h2>
                        <form className="grid gap-3 md:gap-3 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-2">
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("name")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Nombre
                                </label>
                            </div>
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("email")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Correo
                                </label>
                            </div>
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("phone")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Telefono
                                </label>
                            </div>
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("city")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Ciudad
                                </label>
                            </div>

                        </form>
                    </div>
                    <div
                        className="sm:mx-auto max-w-screen-xl sm:px-2 lg:px-12 p-12 sm:p-8  bg-white border border-gray-200 rounded-lg  dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                        <h2 className="mb-4 text-xl font-normal text-gray-900 dark:text-white">Datos
                            Laborales </h2>
                        <form className="grid gap-3 sm:grid-cols-4 md:grid-cols-3 lg:grid-cols-2">
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("companyName")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Compañia
                                </label>
                            </div>
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("bs")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Puesto
                                </label>
                            </div>
                            <div className="relative z-0">
                                <input type="text" id="disabled_standard"
                                       {...register("catchPhrase")}
                                       className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                       disabled/>
                                <label htmlFor="disabled_standard"

                                       className="absolute text-sm text-gray-400 dark:text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto">
                                    Descripción
                                </label>
                            </div>

                        </form>
                    </div>

                </div>
                <h2 className=" mb-4 text-xl mx:text-center font-bold text-gray-900 dark:text-white">Listados de Albunes
                    de <span
                        className="text-blue-600 mb-8">
                    {usuarios.name}
                         </span></h2>
                <TablaIetms columnas={columnas} items={album}/>
            </div>


        </div>)}

    </div>);
}
