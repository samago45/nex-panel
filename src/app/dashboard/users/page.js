"use client"
import React, {useEffect, useState} from "react";
import "react-toastify/dist/ReactToastify.css";
import TablaIetms from "@/components/TablaIetms";
import Paginacion from "@/components/Paginacion";
import Buscador from "@/components/Buscador";
import {UsuarioServices} from "@/app/services/usuarios/UsuarioServices";
import ExportarCSV from "@/components/ExportarCSV";

import {useRouter} from "next/navigation";
import {Spinner} from "flowbite-react";

export default function Page() {
    const router = useRouter();
    const [showModal, setShowModal] = useState(false);
    const [usuarios, setUsuarios] = useState(null);
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true);
        UsuarioServices.obtenerUsuarios()
            .then(response => {
                setUsuarios(response);
            })
            .catch(error => {
                console.error('Error al obtener usuarios:', error);
            }).finally(() => {
            setLoading(false); // Establecer carga como falso después de la petición
        });
    }, []);


    const ItemsTableContainer = () => {
        const [currentPage, setCurrentPage] = useState(1);
        const [searchTerm, setSearchTerm] = useState("");

        const handleChangePage = (page) => {
            setCurrentPage(page);
        };
        let pageSize = 10;
        const startIndex = (currentPage - 1) * pageSize;
        const endIndex = startIndex + pageSize;
        const totalPages = Math.ceil(usuarios.length / pageSize);
        /*    const  currentData = usuarios.slice(startIndex, endIndex);*/

        const handleSelect = (id) => {

            // Navegar a otra página
            router.push(`/dashboard/users/detalles/${id}`);
        }

        const handleSearch = (event) => {
            setSearchTerm(event.target.value);
            setCurrentPage(1);
        };

        const filteredData = usuarios.filter((item) => item.name.toLowerCase().includes(searchTerm.toLowerCase()));
        const currentData = filteredData.slice(startIndex, endIndex)


        const columnas = [{name: "Usuario", selector: (row) => row.username}, {
            name: "Nombre", selector: (row) => row.name
        }, {name: "Correo", selector: (row) => row.email}, {
            name: 'Acciones', selector: (row) => (<>

                <div>
                    <button type="button" onClick={() => handleSelect(row.id)}
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        Ver detalles
                        <svg className="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true"
                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                  d="M1 5h12m0 0L9 1m4 4L9 9"/>
                        </svg>
                    </button>
                </div>
            </>),
        },];


        return (<div className="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
            <div className="flex flex-col md:flex-row items-center justify-between p-4">
                <Buscador searchTerm={searchTerm} handleSearch={handleSearch}/>
                <ExportarCSV data={usuarios}/>
            </div>

            <div className="overflow-x-auto">
                <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <TablaIetms items={currentData} columnas={columnas}/>
                </div>
            </div>
            <Paginacion
                totalPages={totalPages}
                handleChangePage={handleChangePage}
                currentPage={currentPage}
            />

        </div>);
    };


    //main
    return (<section>
        {loading ? (<div
            className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-900 bg-opacity-50">
            <Spinner size="xl"/>
        </div>) : (<div
            className={` max-w-screen-xl mx-auto`}>
            {usuarios ? <ItemsTableContainer/> : <TablaIetms/>}
        </div>)}

    </section>);
}
