const ignorePaths = [
    "/login",

];


const  generateRandomColors  =(count) => {
    const colors = [];
    for (let i = 0; i < count; i++) {
        colors.push({
            r: Math.floor(Math.random() * 256),
            g: Math.floor(Math.random() * 256),
            b: Math.floor(Math.random() * 256)
        });
    }
    return colors;
}

export { ignorePaths , generateRandomColors };
